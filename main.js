$(document).ready(function () {
  $(document).on('click', '#unhide-navbar', function () {
    $('.navbar-menu').addClass('navbar-menu-unhide');
  });

  $(document).on('click', '#hide-navbar', function () {
    $('.navbar-menu').removeClass('navbar-menu-unhide');
  });

  $(document).on('click', '#play-video-btn', function () {
    $('.modal').addClass('modal-unhide');
  });
  $(document).on('click', '.btn-close', function () {
    $('.modal').removeClass('modal-unhide');
  });
})

$('.owl-carousel').owlCarousel({
  loop: false,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1200: {
      items: 2
    },
    1201: {
      items: 3
    }
  }
})